<?php
/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @since             1.0.0
 * @package           Call_Back_BOT
 *
 * @wordpress-plugin
 * Plugin Name:       CallBack to bot telegram
 * Description:       Позволяет отправить с сайта сообщение в социальные сети.
 * Version:           1.0.0
 * Author:            Dolina IS
 * Author URI:        https://dolinais.ru
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       di
 * Domain Path:       /languages
 */

if(!defined('ABSPATH')) {
	exit;
}

require_once dirname( __FILE__ ) . '/config.php';

class di_Loader {
	public $config = [
		'name' => 'DOLINA IS',
		'version' => '1.0.0',
		'textDomain' => 'di',
		'textDomainPath' => 'languages',
		'slug' => 'dolina-is',
		'prefix' => 'di',
		'prefixSeparator' => '_',
	];

	//
	public function __construct() {
		self::init();

		add_action('init', [$this, 'load_plugin_textdomain']);
		self::start();
	}

	/**
	 * Initialize the plugin
	 */
	private function init() {
		if(explode('.', PHP_VERSION)['0'] < 8 ){
		    echo 'Ваша версия PHP ниже 8, плагин не поддерживатеся!';
		}else{
			new app\controllers\IndexController();
			new app\controllers\MenuController();
			new app\controllers\RestController();
			new app\shortcode\IndexShortcode();
		}
	}
	/**
	 * Initialize internationalization (i18n) for this plugin.
	 *
	 * You need to create .mo files to use another languages.
	 * Steps:
	 * 1. Download and setup a free tool like Poedit.
	 * 2. Open .pot file under the plugin's language folder.
	 *    (in Poedit select "New from POT/PO file..." menu)
	 * 3. Choose translation language
	 * 4. Translate texts
	 * 5. Generate .mo file
	 *    (in Poedit just click Save)
	 * 6. Rename generated .mo file to "dolina-is-language_local.mo"
	 *    e.g. "dolina-is-de_DE.mo"
	 * 7. DONE
	 *
	 * For testing change your WordPress language to the translated one
	 * in the WordPress Admin, under Settings/General/Site Language
	 *
	 * Resources:
	 * http://codex.wordpress.org/I18n_for_WordPress_Developers
	 */
	function load_plugin_textdomain() {
		$domain			= $this->config['textDomain'];
		$plugin_folder	= plugin_dir_path(__FILE__);
		$lang_folder	= $plugin_folder . '/' . $this->config['textDomainPath'];
		$mo_file 		= $lang_folder . '/' . $this->config['slug'] . '-' . get_locale() . '.mo';

		load_textdomain($domain, $mo_file);
		load_plugin_textdomain($this->config['textDomain'], false, $lang_folder . '/');
	}


	/**
	 * Deactivates the plugin due to the missing requirements.
	 */
	public function deactivate() {
		if(current_user_can('activate_plugins') && is_plugin_active(plugin_basename(__FILE__))) {
			deactivate_plugins(plugin_basename(__FILE__));

			// Hide the default "Plugin activated" notice
			if(isset($_GET['activate'])) {
				unset($_GET['activate']);
			}
		}
	}

	/**
	 * Starting the plugin.
	 */
	public function start() {
		require_once plugin_dir_path(__FILE__) . 'classes/plugin.php';
		$plugin = new di_Plugin($this->config);

		register_activation_hook(__FILE__, [&$plugin, 'activate']);
		register_deactivation_hook(__FILE__, [&$plugin, 'deactivate']);
	}
}

$di = new di_Loader();

// add_action( 'wp_login', function(){
//      nocache_headers();
//     wp_clear_auth_cookie();
//     $current_logged_user = get_current_user_id();
//     wp_set_auth_cookie($current_logged_user, true);
// }, 10, 2 );

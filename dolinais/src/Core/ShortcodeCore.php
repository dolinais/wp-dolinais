<?php
namespace app\shortcodeCore;

defined( 'ABSPATH' ) || die( '-1' );

class ShortcodeCore {

	public function __construct($get_class_name, $__view, $data=[], $nameshortcode='shortcode'){
        $this->FilesExit(str_replace("\\", "/", $get_class_name), $__view);
        add_shortcode($nameshortcode,
            function( $atts ) use ($get_class_name, $__view, $data ) {
                $atts = shortcode_atts( $data, $atts );
                if (!empty($atts)) extract($atts);
                if (file_exists(TEMPLATES . '/'.str_replace("\\", "/", $get_class_name).'/'.$__view.'.php')) {
                    ob_start();
                        require TEMPLATES . '/'.str_replace("\\", "/", $get_class_name).'/'.$__view.'.php';
                    $output = ob_get_contents();
                    ob_end_clean();

                    return $output;
                }
            }
        );
  	}

    function FilesExit($get_class_name, $__view){
        if (!file_exists(TEMPLATES . '/'.$get_class_name)){
            mkdir(TEMPLATES . '/'.$get_class_name, 0755, true);
        }
        if (!file_exists(TEMPLATES . '/'.$get_class_name.'/'.$__view.'.php')) {
            var_dump(TEMPLATES . '/'.str_replace("\\", "/", $get_class_name).'/'.$__view.'.php');
            file_put_contents(TEMPLATES . '/'.$get_class_name.'/'.$__view.'.php', '<div>HTML code</div>');
        }
    }
}

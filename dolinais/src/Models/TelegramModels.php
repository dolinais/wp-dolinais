<?
namespace app\models;

class TelegramModels{

	public function __construct(){}
  	public function send($method, $response){
  		global $wpdb;
		$tablename = $wpdb->prefix."dolinais_callback";
		$accessToken = $wpdb->get_results("SELECT * FROM ".$tablename." WHERE name='telegram'");
		$ch = curl_init('https://api.telegram.org/bot' . $accessToken[0]->accessToken . '/' . $method);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $response);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HEADER, false);
        $res = curl_exec($ch);
        curl_close($ch);
        return json_decode($res, true)['ok'];
  	}
}
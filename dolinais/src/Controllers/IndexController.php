<?php

namespace app\controllers;

defined( 'ABSPATH' ) || die( '-1' );

class IndexController {

	function __construct(){
		add_action('wp_enqueue_scripts', array($this, 'bootstrap_init'));
		render(get_class($this), 'index', null, 'wp_footer');
  	}

	function bootstrap_init() {
		wp_enqueue_script( 'apiscript', RESOURCES . '/js/script.js', array(), '1.0.1' );
	    wp_enqueue_style( 'bootstrap@5.2.3', RESOURCES.'/style/npm_bootstrap@5.3.0_dist_css_bootstrap.min.css', array(), '5.3.0' );
	    wp_enqueue_style( 'popap_tsyle', RESOURCES.'/style/popap_style.css', array(), '1.0.0' );
	    wp_enqueue_script( 'bootstrap@5.3.0_dist_js', RESOURCES . '/js/bootstrap@5.3.0_dist_js_bootstrap.bundle.min.js', array(), '5.3.0' );
	}
}
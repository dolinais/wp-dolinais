<div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Редактировать ключ</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Закрыть"></button>
      </div>
      <div class="modal-body">
    <form id="form-data" onsubmit="
        var form = document.getElementById('form-data');
        SendDataForm('res', '/api/dolinais/v2/editekey', JSON.stringify({'response': {
             'accessToken': form.accessToken.value,
             'telegram_id': form.telegram_id.value
        } }), true);
        return false;
        ">
        <div class="mb-3">
          <label for="exampleFormControlInput1" class="form-label">TELEGRAM ID</label>
          <input type="text" name="telegram_id" class="form-control" id="exampleFormControlInput1" placeholder="000000000">
        </div>
        <div class="mb-3">
          <label for="exampleFormControlTextarea1" class="form-label">Вставьте ключ бота телеграм</label>
          <textarea name="accessToken" class="form-control" id="exampleFormControlTextarea1" rows="2" placeholder="992927e5b1c8a237875c70a302a34e22"></textarea>
        </div>
        <div class="modal-footer">
            <button type="submit" id="submit" class="btn btn-primary">Отправить</button>
          </div>
    </form>
    <div id="res"></div>
      </div>
    </div>
  </div>
</div>
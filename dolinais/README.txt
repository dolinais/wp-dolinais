=== DOLINA IS ===
Contributors: dolinais
Donate link: https://dolinais.ru/about
Tags: telegram, callback
Requires at least: 6.2.2
Stable tag: 1.0.0
Requires PHP: 8
License: GPLv2 or later
License URI: https://www.gnu.org/licenses/gpl-2.0.html

С помощью данного плагина можно принимать заявки с сайта в телеграм.

== Description ==

С помощью данного плагина можно принимать заявки с сайта в телеграм.

== Installation ==

Первое, что нужно сделать -> настроить постоянные ссылки. Структура постоянных ссылок -> установить на "Название записи" элемента %postname%

Получить токен для существующего бота можно в чате с ботом BotFather с помощью команды /token. Если у вас создано несколько ботов, укажите для какого именно бота нужен токен. Токен должен быть следующего вида: 6130123832:AAGJPJ88dgq4y5z999H8o81zhVSSDcxeag

Ниже вставьте токен и id пользователя (узнать свой id) куда будут приходить сообщения.

По всем вопросам пишите:

== Frequently Asked Questions ==

= Получить токен =

Получить токен для существующего бота можно в чате с ботом BotFather с помощью команды /token. Если у вас создано несколько ботов, укажите для какого именно бота нужен токен. Токен должен быть следующего вида: 6130123832:AAGJPJ88dgq4y5z999H8o81zhVSSDcxeag


== Screenshots ==

1. Home site `/assets/callback.png`
2. Install `/assets/plugininstall.png`
3. Admin plugin `/assets/pluginadmin.png`

== Changelog ==


== Upgrade Notice ==


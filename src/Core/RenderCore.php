<?php
namespace app\renderCore;

defined( 'ABSPATH' ) || die( '-1' );
class RenderCore {

	public function __construct($get_class_name, $__view, $data=[], $template='wp_footer'){
        $this->FilesExit(str_replace("\\", "/", $get_class_name), $__view);
        if(!$template){
            require TEMPLATES . '/'.str_replace("\\", "/", $get_class_name).'/'.$__view.'.php';
        } else{
            add_action($template,
                function() use ($get_class_name, $__view, $data ) {
                    $this->render(str_replace("\\", "/", $get_class_name), $__view, [
                    'data' => $data
                ]);
            });
        }
  	}
	function render($get_class_name, $__view, $params = []){
        if (!empty($params)) extract($params);

        if (file_exists(TEMPLATES . '/'.$get_class_name.'/'.$__view.'.php')) {
             ob_start();
                 require TEMPLATES . '/'.$get_class_name.'/'.$__view.'.php';
             ob_get_flush();
        }
    }
    function FilesExit($get_class_name, $__view){
        if (!file_exists(TEMPLATES . '/'.$get_class_name)){
            mkdir(TEMPLATES . '/'.$get_class_name, 0755, true);
        }
        if (!file_exists(TEMPLATES . '/'.$get_class_name.'/'.$__view.'.php')) {
            var_dump(TEMPLATES . '/'.str_replace("\\", "/", $get_class_name).'/'.$__view.'.php');
            file_put_contents(TEMPLATES . '/'.$get_class_name.'/'.$__view.'.php', '<div>HTML code</div>');
        }
    }
}

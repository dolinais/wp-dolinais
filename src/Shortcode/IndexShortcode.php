<?php

namespace app\shortcode;

defined( 'ABSPATH' ) || die( '-1' );

class IndexShortcode {

	function __construct() {
		shorcoderender(get_class($this), 'shortcode', array(
			'forname' => 'Как с Вами можно связаться?',
			'text' => 'Ваш вопрос'
		), 'shortcode');
    }
}
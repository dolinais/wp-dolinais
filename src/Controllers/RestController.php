<?php

namespace app\controllers;

use WP_REST_Server;

defined( 'ABSPATH' ) || die( '-1' );

class RestController {

	function __construct(){
		add_filter( 'rest_url_prefix', array($this, 'change_rest_api_prefix') );
		add_action( 'rest_api_init', function(){
			register_rest_route( 'dolinais/v2', '/editekey', array(
				'methods'  => WP_REST_Server::CREATABLE,
				'callback' => function(){
					$this->settings_form(json_decode(file_get_contents('php://input'), true));
				},
				'permission_callback' => function(){
					if ( isset($_SERVER["HTTP_REFERER"]) && $_SERVER["HTTP_REFERER"] ===  site_url().'/wp-admin/admin.php?page=dolinais') {
						return true;
					}else{
						return false;
					}
				},
			) );
			register_rest_route( 'dolinais/v2', '/send', array(
				'methods'  => WP_REST_Server::CREATABLE,
				'callback' => function(){
					$this->send_form_rest(json_decode(file_get_contents('php://input'), true));
				},
				'permission_callback' => function(){
					return true;
				},
			) );
		} );
  	}

  	function change_rest_api_prefix( $slug ){
		return 'api';
	}

	function send_form_rest($data){
		sleep(10);
		if ($_SERVER['REQUEST_METHOD'] === 'POST' && isset($data['id']) && isset($data['response']['userdata'])){
			global $wpdb;
			$tablename = $wpdb->prefix."dolinais_callback";
			$datatg = $wpdb->get_row( "SELECT * FROM $tablename WHERE name = 'telegram'" );
			if($data['id'] == '' && $data['response']['userdata'] == ''){
		    	echo wp_json_encode(array(
			        "data" => null,
			        "status" => 'Ошибка: пустые поля!'
			    ));
			    exit();
		    }
		    tg($datatg->telegram_id, 'От: '.$data['id']."\n".'Сообщение: '.$data['response']['userdata']);
            echo wp_json_encode(array(
		        "data" => null,
		        "status" => 'Мы получили ваше сообщение, скоро с Вами свяжемся!'
		    ));
		    exit();
		}else{
			echo wp_json_encode(array(
		        "data" => null,
		        "status" => 'Ошибка!'
		    ));
		    exit();
		}
	}
	function settings_form($data){
		sleep(3);
		if ($_SERVER['REQUEST_METHOD'] === 'POST' && isset($data['response']['accessToken']) && isset($data['response']['telegram_id'])){
		    global $wpdb;
		    $accessToken = $data['response']['accessToken'];
		    $telegram_id = $data['response']['telegram_id'];
		    $tablename = $wpdb->prefix."dolinais_callback";
		    foreach($wpdb->get_results( "SELECT accessToken,telegram_id FROM $tablename" ) as $value){
				$value = wp_json_encode($value) ;
			}
		    if($accessToken != ''){
		    	$context = stream_context_create(['http' => ['ignore_errors' => true]]);
				$result = wp_remote_get("https://api.telegram.org/bot".$accessToken."/getMe", false, $context);
		    	if($http_response_header[0] === 'HTTP/1.1 401 Unauthorized'){
				    echo wp_json_encode(array(
				        "data" => $value,
				        "status" => 'Ошибка, неверно указан ключ: '.$http_response_header[0]
				    ));
				    exit();
				}
				if($http_response_header[0] === 'HTTP/1.1 404 Not Found'){
				    echo wp_json_encode(array(
				        "data" => $value,
				        "status" => 'Ошибка, неверно указан ключ: '.$http_response_header[0]
				    ));
				    exit();
				}
		        $wpdb->update(
					$wpdb->prefix . 'dolinais_callback',
					array('accessToken' => $accessToken),
					array('id' => 1),
					array( '%s' ),
					array('%d',	'%s' )
				);
		    }
		    if($telegram_id != ''){
		    	if(tg($telegram_id, 'Успешно, бот активирован!')){
		    		$wpdb->update(
						$wpdb->prefix . 'dolinais_callback',
						array('telegram_id' => $telegram_id),
						array('id' => 1),
						array( '%s' ),
						array('%d',	'%s' )
					);
		    	}else{
		    		echo wp_json_encode(array(
				        "data" => $value,
				        "status" => 'Ошибка: На этот ID не удаётся отправить сообщение.'
				    ));
				    exit();
		    	}
		    }
		    if($accessToken == '' && $telegram_id == ''){
		    	echo wp_json_encode(array(
			        "data" => $value,
			        "status" => 'Ошибка: пустые поля'
			    ));
			    exit();
		    }
		    foreach($wpdb->get_results( "SELECT accessToken,telegram_id FROM $tablename" ) as $value){
				$value = wp_json_encode($value) ;
			}
			echo wp_json_encode(array(
		        "data_id" => 'accessToken',
		        "data" => $value,
		        "status" => 'Успешно!'
		    ));
		    exit();
		}else{
			echo wp_json_encode(array(
		        "data" => null,
		        "status" => 'Ошибка'
		    ));
		}
	}
}
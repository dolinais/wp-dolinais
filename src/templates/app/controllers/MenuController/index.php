<?php
if ( ! defined( 'ABSPATH' ) ) exit;
global $wpdb;
$tablename = $wpdb->prefix."dolinais_callback";
$data = $wpdb->get_row( "SELECT * FROM $tablename WHERE name = 'telegram'" );
?>
<div class="container">
    <h1>Настройки telegram</h1>
    <p class="lh-sm">Первое что нужно сделать -> настроить постоянные ссылки. Нажмите <a href="/wp-admin/options-permalink.php" class="text-primary" target="_blank">сюда</a>. Структура постоянных ссылок -> установить на "Название записи" элемента %postname%</p>

    <p class="lh-sm">Получить токен для существующего бота можно в чате с ботом <a href="https://t.me/BotFather" class="text-primary" target="_blank">BotFather</a> с помощью команды /token. Если у вас создано несколько ботов, укажите для какого именно бота нужен токен. Токен должен быть следующего вида: 6130123832:AAGJPJ88dgq4y5z999H8o81zhVSSDcxeag</p>

    <p class="lh-sm">Ниже вставьте токен и id пользователя (<a href="https://t.me/my_id_bot" class="text-primary" target="_blank">узнать свой id</a>) куда будут приходить сообщения.</p>

    <p class="lh-sm">По всем вопросам пишите: <a href="https://t.me/devdolinais" class="text-primary" target="_blank">сюда</a></p>

    <p class="lh-sm"><b><a href='#' data-bs-toggle='modal' data-bs-target='#exampleModal'>Редактировать</a></b></p>
    <table class="table">
        <thead>
        <tr>
            <th scope="col">Токен</th>
            <th scope="col">TELEGRAM USER ID</th>
            <th scope="col"></th>
        </tr>
        <tr>
            <td data-id="accessToken" id='data-res'><?php echo $data->accessToken ?? null ; ?></td>
            <td data-id="telegram_id" id='data'><?php echo $data->telegram_id ?? null ; ?></td>
            <td><a href='#' data-bs-toggle='modal' data-bs-target='#exampleModal'>Редактировать</a></td>
        </tr>
         <tbody>
    </table>
</div>
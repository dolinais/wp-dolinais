<div class="modal-dialog">
    <div class="modal-content">
  		<div class="modal-body">
		    <form id="form-data" onsubmit="
		        var form = document.getElementById('form-data');
		        SendDataForm('res', '/api/dolinais/v2/send', JSON.stringify({ 'id': form.username.value, 'response': { 'userdata': form.usertext.value } }), true);
		        return false;
		        ">
		        <div class="mb-3">
		          <label for="exampleFormControlInput1" class="form-label"><?php echo $atts['forname']; ?></label>
		          <input type="text" name="username" class="form-control" id="exampleFormControlInput1" placeholder="почта, номер телефона">
		        </div>
		        <div class="mb-3">
		          <label for="exampleFormControlTextarea1" class="form-label"><?php echo $atts['text']; ?></label>
		          <textarea name="usertext" class="form-control" id="exampleFormControlTextarea1" rows="2" placeholder="<?php echo $atts['text']; ?>"></textarea>
		        </div>

		        <div class="modal-footer">
		            <button type="submit" id="submit" class="btn btn-primary">Отправить</button>
		          </div>
		    </form>
			<div id="res"></div>
			<div id='data-res'></div>
      	</div>
    </div>
</div>
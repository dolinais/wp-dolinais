<?php
if ( ! defined( 'ABSPATH' ) ) exit;
/**
 * Setup functions on activate & deactivate events:
 * - Initialize custom options, database, etc.
 * - Upgrade custom options, database, etc.
 * - Cleanup on deactivate
 */
require_once plugin_dir_path(dirname(__FILE__)) . 'classes/base.php';

class di_Setup extends di_Base {

	/**
	 * Specify all codes required for plugin activation here.
	 */
	public function activate() {
		// Initialize custom things on plugin activation
		$this->install();
	}

	/**
	 * Specify all codes required for plugin deactivation here.
	 */
	public function deactivate() {
	}

	/**
	 * Specify all codes required for plugin uninstall here.
	 *
	 */
	public function uninstall() {
		global $wpdb;
		$tablename = $wpdb->prefix . "dolinais_callback";
		$sql = "DROP TABLE IF EXISTS $tablename;";
		if($wpdb->prepare($sql)){
			delete_option("dolina-is");
		}
	}

	public function install() {

		// Initialize plugin options
		$this->initOptions();
	}

	/**
	 * Storing custom options
	 */
	public function initOptions() {
		require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
		global $wpdb;
	    $charset_collate = $wpdb->get_charset_collate();
	    $tablename = $wpdb->prefix . "dolinais_callback";
	    $sql = "CREATE TABLE $tablename (
			id int(11) NOT NULL AUTO_INCREMENT,
			name varchar(255) NOT NULL,
			accessToken varchar(255) NOT NULL,
			telegram_id INT NOT NULL,
			status INT NOT NULL,
			created_at INT NOT NULL,
			PRIMARY KEY  (id)
	  	) $charset_collate;";
	    dbDelta($sql);
	    if(!$wpdb->get_row( "SELECT * FROM $tablename WHERE name = 'telegram'" )){
		    $wpdb->insert(
		        $wpdb->prefix . 'dolinais_callback',
		        array('name' => 'telegram', 'accessToken' => 'Token'),
		        array( '%s' ),
		        array('%d', '%s' )
		    );
		}
	}
}
